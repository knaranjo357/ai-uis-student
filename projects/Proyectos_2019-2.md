---
# Proyectos 2019-1. Inteligencia Artificial. 

## Prof: Fabio Martínez, Ph.D
## Prof: Gustavo Garzón
---

# Lista de Proyectos
1. [Predicción de eventos extremos de precipitación](#proy1)
2. [Análisis estadístico sobre Covid-19](#proy2)
3. [IA_chest-xray-pneumonia: predicción de neumonía en Rx](#proy3)
4. [Identificación de dolencias a partir de información suministrada por pacientes mediante audio y texto](#proy4)
5. [TSIGN: reconocimiento de lenguaje de señas americano](#proy5)
6. [Clasificación de imágenes de retinopatía diabética de acuerdo a su gravedad](#proy6)
7. [Clasificador Musical](#proy7)
8. [Estudio de delitos sexuales en Colombia](#proy8)
9. [Accidentes de Transito y Costos de Atencion Hospitalaria](#proy9)
10. [Proyección de costos de la atención hospitalaria en Bucaramanga por accidentes de tránsito](#proy10)
11. [Radiación Solar](#proy11)
12. [Reconocimiento de emociones por voz](#proy12)
13. [Análisis de calidad de vida en Colombia](#proy13)
14. [Proyecto Next Earthquake](#proy14)
15. [Análisis de pacientes y sus principales causas de afecciones](#proy15)
---

## Predicción de eventos extremos de precipitación <a name="proy1"></a>

**Autores: Danilo Andres Suarez Higuita**

<img src="https://raw.githubusercontent.com/dasuarezh/proyecto_IA/master/Banner.png" style="width:700px;">

**Objetivo: Predecir la precipitación en la región de Santander utilizando modelos de machine learning**

- Dataset: Modelo ER5, Modelo satelital IMERGm dataset UNGRD
- Modelo: Regresores basados en redes neuronales y Arboles de decisión


[(code)](https://github.com/dasuarezh/proyecto_IA) [(video)](https://github.com/dasuarezh/proyecto_IA/blob/master/video_ia.avi) [(+info)](https://github.com/dasuarezh/proyecto_IA/blob/master/Presentacion.pdf)

---

## Análisis estadístico sobre Covid-19 <a name="proy2"></a>

**Autores: Amaya Garcia Luis Miguel - Sanmiguel Arenas Camilo Enrique**

<img src="https://gitlab.com/bivl2ab/academico/cursos-uis/ai/ai-uis-student/-/raw/master/projects/sources/2019-2/2170110-2160414/2170110-2160414.jpg" style="width:700px;">

**Objetivo: Predecir el estado vital de pacientes con determinada edad. Trazar y analizar comportamientos estadísticos proporcionados por los datos. Implementar el uso de diversos mecanismos vistos en el curso para análisis, predicción y comprobación de acertación en el tratamiento de datos**

- Dataset: Novel Corona Virus 2019 Dataset, DXY.cn
- Modelo: SVC (Support Vector Classifier), KNeighborsClassifier, GaussianNB, DecisionTreeClassifier, RandomForestClassifier


[(code)](https://github.com/camilosanmiguel/IA-covid-Uis) [(video)](https://www.youtube.com/watch?v=lVA7GA7Thyk) [(+info)](https://github.com/camilosanmiguel/IA-covid-Uis/blob/master/DiapositivasIA.pdf)

---
## IA_chest-xray-pneumonia: predicción de neumonía en Radiografias de torax <a name="proy2"></a>

**Autores: Natalia Gómez, Nicolás Ramirez**

<img src="https://raw.githubusercontent.com/nataliaalbiadez/IA_chest-xray-pneumonia/master/Banner.jpg" style="width:700px;">

**Objetivo: Detectar Neumonía Empleando Radiografías de Tórax a través de redes Neuronales**  

- Dataset: Disponible en Kaggle [link](https://www.kaggle.com/paultimothymooney/chest-xray-pneumonia#person1000_bacteria_2931.jpeg)
- Modelo: Redes neuronales profundas


[(code)](https://github.com/nataliaalbiadez/IA_chest-xray-pneumonia) [(video)](https://youtu.be/GbLDiErGXzY) [(+info)](https://github.com/nataliaalbiadez/IA_chest-xray-pneumonia/blob/master/INTELIGENCIA_DIAPO.pdf)

---

## Identificación de dolencias a partir de información suministrada por pacientes mediante audio y texto <a name="proy4"></a>

**Autores: Martinez Estrada Emmanuel David - Riveros Gomez Maria Paula - Serrano Villanova Laura Daniela**

<img src="https://gitlab.com/bivl2ab/academico/cursos-uis/ai/ai-uis-student/-/raw/master/projects/sources/2019-2/2162134-2162113-2162135/2162134-2162113-2162135.jpg" style="width:700px;">

**Objetivo: Clasificar dolencias a partir de la descripción dada por los pacientes a través de los audios y textos.**

- Dataset: Medical Speech, Transcription, and Intent.
- Modelo: Redes neuronales con Tensorflow y Fastai.

[(code)](https://github.com/Enmartz/Clasificacion-de-dolencias-mediante-texto-y-audio) [(video)](https://www.youtube.com/watch?v=MA9LT2hE7s0) [(+info)](https://github.com/Enmartz/Clasificacion-de-dolencias-mediante-texto-y-audio/blob/master/Presentation%20(Diapositivas)/Proyecto_IA.pdf)

---

## TSIGN: reconocimiento de lenguaje de señas americano <a name="proy5"></a>

**Autores: Sofia Torres, Sebastian Garcia, Yeison Valencia, Daniel Ardila**

<img src="https://raw.githubusercontent.com/sofiat99/TSIGN-cv/master/Banner.jpeg" style="width:700px;">

**Objetivo: Reconocer lenguaje de señas**  

- Dataset: Lenguaje de Señas americano
- Modelo: BoW, Backgroun substraction, DNN, CNN, Random forest, SVM


[(code)](https://github.com/sofiat99/TSIGN-cv) [(video)](https://www.youtube.com/watch?v=AGOJ_qRG0Rc) [(+info)](https://www.canva.com/design/DAD2P6_P5zY/c6wfwcfF2UmP0qpoEPR4Xw/view?utm_content=DAD2P6_P5zY&utm_campaign=designshare&utm_medium=link&utm_source=publishsharelink)

---


## Clasificación de imágenes de retinopatía diabética de acuerdo a su gravedad <a name="proy6"></a>

**Autores: Jessica Fernanda Pedraza Cadena**

<img src="https://gitlab.com/bivl2ab/academico/cursos-uis/ai/ai-uis-student/-/raw/master/projects/sources/2019-2/2151853/2151853.jpg" style="width:700px;">

**Objetivo: Se pretende entrenar una red neuronal para que aprenda a clasificar por sí sola si una persona tiene retinopatía diabética y la gravedad o la etapa en la que se encuentra.**

- Dataset: Diabetic Retinopathy 224x224 Gaussian Filtered, dataset.
- Modelo: Redes neuronales con Tensorflow y Keras.

[(code)](https://github.com/jessicapedraza29/Proyecto_IA) [(video)](https://www.youtube.com/watch?v=6LP1-RKJQwQ) [(+info)](https://gitlab.com/bivl2ab/academico/cursos-uis/ai/ai-uis-student/-/raw/master/projects/sources/2019-2/2151853/2151853.pdf)

---

## Clasificador Musical <a name="proy7"></a>

**Autores: David Castro, Wilder Rojas**

<img src="https://raw.githubusercontent.com/DavidCastro-IA/ClasificadorMusical/master/Banner.jpeg" style="width:700px;">

**Objetivo: Clasificar piezas de audio basado en caracteristicas de los generos musicales**  

- Dataset: Opihi, Kaggle [link](http://opihi.cs.uvic.ca/sound/genres.tar.gz)
- Modelo: DNN, non-supervised, RF


[(code)](https://github.com/DavidCa) [(video)](https://www.youtube.com/watch?v=boDPZP72) [(+info)](https://github.com/DavidCastro-IA/ClasificadorMusical/blob/master/Presentaci%C3%B3n.pptx)

---

## Estudio de delitos sexuales en Colombia <a name="proy8"></a>

**Autores: Juan Pablo Viviescas - Sergio Dulcey - Eduard Caballero**

<img src="https://raw.githubusercontent.com/eduardcaballero/DelitosSexuales/master/NoteBook/delitos_sexuales.jpg" style="width:700px;">

**Objetivo: Realizar una predicción del dia de la semana, hora y municipio en que ocurre un posible delito sexual, por medio de un análisis de datos proporcionados por la policía nacional de Colombia.**

- Dataset: Delitos sexuales 2019, Datos abiertos Colombia.
- Modelo: DecisionTreeClassifier, RandomForestClassifier, KNeighborsClassifier y XGBClassifier.

[(code)](https://github.com/eduardcaballero/DelitosSexuales) [(video)](https://www.youtube.com/watch?v=ZKe4nODYyuA) [(+info)](https://github.com/eduardcaballero/DelitosSexuales/blob/master/Presentation/Delitos_sexuales.pdf)

---

## Accidentes de Transito y Costos de Atencion Hospitalaria <a name="proy9"></a>

**Autores: Elkin Fernandez, Jeferson Dominguez, Christian Camacho**

<img src="https://www.policia.gov.co/sites/default/files/styles/2000/public/foto-direccion-transito-y-transporte.jpg?itok=pBKlsdpz" style="width:400px;">

**Objetivo: Estimar los riesgos de gasto hospitalario debido a los accidentes de transito**  

- Dataset: dataset que presentaba costos hospitalarios en la ciudad de Bucaramanga, acompañado de un registro de 19643 casos, en campos tales como: edad, sexo, fecha del accidente, costos, EPS, entre otros [data](https://www.datos.gov.co/Salud-y-Protecci-n-Social/Costos-de-la-atenci-n-hospitalaria-en-Bucaramanga-/g4vd-w4ip)
- Modelo: SVM, DNN, RF


[(code)](https://gitlab.com/elkinfernandez21/proyectoartificial/-/blob/master/ProyectoFinal2.ipynb) [(video)](https://drive.google.com/drive/folders/1JvxQLWlzmajyCjHbcxwWpkZsRhxNlswq) [(+info)](https://gitlab.com/elkinfernandez21/proyectoartificial/-/blob/master/Video/EstimadorCostos.pptx)

---

## Proyección de costos de la atención hospitalaria en Bucaramanga por accidentes de tránsito <a name="proy10"></a>

**Autores: Jonattan Stivent Vargas Camacho - Jonathan Buitrago**

<img src="https://gitlab.com/bivl2ab/academico/cursos-uis/ai/ai-uis-student/-/raw/master/projects/sources/2019-2/2163024-2142612/2163024-2142612.jpg" style="width:700px;">

**Objetivo: Prestar un servicio para predecir con la mayor exactitud posible el costo de la atención hospitalaria de accidentes de tránsito en Bucaramanga.**

- Dataset: Costos de la atención hospitalaria en Bucaramanga por accidentes de tránsito enero 2018 a octubre 2019, Datos abiertos Colombia.
- Modelo: DecisionTreeRegressor, RandomForestRegressor, SVR, KMeans.

[(code)](https://github.com/J0bU/aiUIS) [(video)](https://www.youtube.com/watch?v=aVp1CeQiuww) [(+info)](https://gitlab.com/bivl2ab/academico/cursos-uis/ai/ai-uis-student/-/raw/master/projects/sources/2019-2/2163024-2142612/2163024-2142612.pdf)

---

## Radiación Solar <a name="proy11"></a>

**Autores: Jhoan Diaz, Nestor Infante, Daniel Perez**

<img src="https://raw.githubusercontent.com/NesttorIvan/IA-Project/master/imgs/Banner1.png" style="width:400px;">

**Objetivo: Predecir el indice de radiación**  

- Dataset: [disponible](https://github.com/NesttorIvan/IA-Project/tree/master/data/csv)
- Modelo: SVM, DT, RF

[(code)](https://github.com/NesttorIvan/IA-Project) [(video)](https://drive.google.com/drive/folders/1luk7rIgQaLTVv2VMBDKHrQlUd-zOL9Y-?usp=sharing
) [(+info)](https://github.com/NesttorIvan/IA-Project/blob/master/RadiacionSolar-IA.pdf)

---

## Reconocimiento de acciones por voz <a name="proy12"></a>

**Autores: Mario Hernan Vallejo Huertas**

<img src="https://raw.githubusercontent.com/mayito123/Reconocimiento-de-emociones-por-voz-para-la-determinaci-n-de-la-validez-de-llamadas-de-emergencia-me/master/banner.jpg" style="width:700px;">

**Objetivo: Identificar emociones humanas a partir de grabaciones de voz**  

- Dataset: CREMAD4 con 7442 audios y 91 actores
- Modelo: DNN

[(code)](https://github.com/mayito123/Reconocimiento-de-emociones-por-voz-para-la-determinaci-n-de-la-validez-de-llamadas-de-emergencia-me) [(video)](https://github.com/mayito123/Reconocimiento-de-emociones-por-voz-para-la-determinaci-n-de-la-validez-de-llamadas-de-emergencia-me/blob/master/Video.mp4) [(+info)](https://github.com/mayito123/Reconocimiento-de-emociones-por-voz-para-la-determinaci-n-de-la-validez-de-llamadas-de-emergencia-me/blob/master/Presentaci%C3%B3n.pptx.pdf)

---

## Análisis de calidad de vida en Colombia <a name="proy13"></a>

**Autores: Iván Castillo - Juan Castellanos - Gabriela Vega**

<img src="https://gitlab.com/bivl2ab/academico/cursos-uis/ai/ai-uis-student/-/raw/master/projects/sources/2019-2/2162119-2160023-2161671/2162119-2160023-2161671.jpg" style="width:700px;">

**Objetivo: Clasificar por vivienda y servicios básicos las características que influyen en la calidad de vida de los hogares colombianos.**  

- Dataset: Cerca 88k registros de respuestas en la Encuesta de Calidad de Vida (DANE) y 35 caracteristicas, entre las cuales están tipo de vivienda y estrato.
- Modelo: Clasificadores - Random Forest, Support Vectorial Machine, Decision Tree

[(code)](https://github.com/ivanrcas/ACV_ai_project/blob/master/proyecto/ACV_ai_project.ipynb) [(video)](https://www.youtube.com/watch?v=zqZ8ipQfqRk) [(+info)](https://github.com/ivanrcas/ACV_ai_project/blob/master/proyecto/presentacion_ACV_ai_project.pdf)

---

## Proyecto Next Earthquake <a name="proy14"></a>

**Autores: Oscar Gil Gamez - Hordan Navarro - Jhon Parra Rodriguez**

<img src="https://gitlab.com/bivl2ab/academico/cursos-uis/ai/ai-uis-student/-/raw/master/projects/sources/2019-2/2161327-2131799-2161336/2161327-2131799-2161336.jpg" style="width:700px;">

**Objetivo: predecir futuros terremotos para proteger a las comunidades donde se encuentra el sismo.**

- Dataset: Reporte de Sismicidad del Servicio Geológico Colombiano-Año 2015, Datos abiertos Colombia
- Modelo: DecisionTreeRegressor, RandomForestRegressor, LogisticRegressor, SVR

[(code)](https://github.com/oscarandres16/nexthearthquake) [(video)](https://www.youtube.com/watch?v=dmtA5YXZE4c) [(+info)](https://gitlab.com/bivl2ab/academico/cursos-uis/ai/ai-uis-student/-/blob/master/projects/sources/2019-2/2161327-2131799-2161336/2161327-2131799-2161336.pdf)

---

## Análisis de pacientes y sus principales causas de afecciones <a name="proy15"></a>

**Autores: Sebastián Cárdenas Acevedo - Javier David Landazabal Landazabal**

<img src="https://gitlab.com/bivl2ab/academico/cursos-uis/ai/ai-uis-student/-/raw/master/projects/sources/2019-2/2161062-2151482/2161062-2151482.jpg" style="width:700px;">

**Objetivo: este proyecto propone una manera rápida y efectiva de poder prevenir los padecimientos de los pacientes de manera que puede facilitar el trabajo de miles de médicos que a diario exponen sus vidas.**

- Dataset: Atención médica 2016, Datos abiertos Colombia.
- Modelo: RandomForestClassifier, GaussianNB, Support Vector Machine

[(code)](https://github.com/LandaAzul/proyectoIA) [(video)](https://www.youtube.com/watch?v=yZ9oFdyTb4c) [(+info)](https://gitlab.com/bivl2ab/academico/cursos-uis/ai/ai-uis-student/-/blob/master/projects/sources/2019-2/2161062-2151482/2161062-2151482.pdf)

